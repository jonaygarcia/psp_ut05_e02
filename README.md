# GPG

![img_01][img_01]

Hemos hablado ya sobre la criptografía _simétrica_ y _asimétrica_ y sobre la firma digital, solo toca ponerlo en práctica con __GnuPG__ (la versión libre de _PGP_ o mejor dicho _Pretty Good Privacy_), herramienta con el que cifraremos cualquier tipo de archivo que podremos enviar con cierta seguridad de que nadie lo podrá leer

## ¿Qué es GnuPG?

Antes de empezar con lo interesante tenemos que saber que es __GPG__ (_GNU Privacy Guard_), que es un derivado libre de _PGP_ y __su utilidad es la de cifrar y firmar digitalmente__, siendo además multiplataforma (podéis descargarlo desde la página oficial) aunque viene incorporado en algunos sistemas Linux, como en Ubuntu.

## Anillo de claves

_GPG_ tiene un repositorio de claves (anillo de claves) donde almacena todas las llaves que tenemos almacenadas en nuestro sistema, ya sean privadas o públicas (con la clave pública cifraremos un mensaje que solo podrá descifrar aquel que posea la clave privada).

Más adelante cuando veamos un anillo de claves debemos de recordar que pub hace referencia a la clave pública y sub hace referencia a la privada (y que tenemos que tener a buen recaudo).

## Servidores de claves

Para que nos cifren un mensaje tenemos que compartir la clave pública de nuestro par de claves para cifrar, y como es un poco engorroso difundir una clave a muchas personas existen los servidores de claves PGP (compatibles con GPG), donde subiré una clave pública para el que quiera probar los ejemplos.

Ejemplos de servidores dond epuedo alojar mis claves públicas GPG:

* __pgp.rediris.es__: español, falla algunas veces.
* __pgp.mit.edu__: americano, del MIT y no suele dar problemas.

## Cifrado simétrico

![img_02][img_02]

Como ya sabéis el cifrado simétrico es el tipo de cifrado más sencillo que hay, es más rápido de procesar y por desgracia menos seguro que el cifrado asimétrico.

Para empezar tenemos que tener un archivo de cualquier tipo e introducir en la terminal de Linux el comando _gpg_ con el parámetro '-c' para cifrar y '-d' para descifrar.

```bash
cfgs@ubuntu:~$ echo "Hello World!" > greeting.txt
cfgs@ubuntu:~$ gpg -c greeting.txt
```

Al ejecutar el comando __gpg -c [archivo]__ nos pide una contraseña que usará para cifrar el mensaje. Al finalizar, genera el archivo encriptado _greeting.txt.gpg_

```bash
cfgs@ubuntu:~$ ls -l
total 8
-rw-rw-r-- 1 cfgs cfgs 13 dic 19 12:38 greeting.txt
-rw-rw-r-- 1 cfgs cfgs 95 dic 19 13:53 greeting.txt.gpg
```

 Para descrifrarlo usaremos el comando __gpg -d [archivo]__, nos solicitará la clave que usamos para encriptar el fichero:

 ```bash
cfgs@ubuntu:~$ gpg -d greeting.txt.gpg
gpg: datos cifrados AES
gpg: el agente gpg no esta disponible en esta sesión
gpg: cifrado con 1 contraseña
Hello World!
```

## Cifrado asimétrico

![img_03][img_03]

### Solucionar problema de bits aleatorios disponibles en Ubuntu 16.04

A la hora de genera las claves, para evitar el error:

    No hay suficientes bytes aleatorios disponibles. Haga algún otro trabajo para que el sistema pueda recolectar más entropía (se necesitan 238 bytes más).

Debemos instalar las __rng-tools__. Para ello ejecutar el siguinente comando:

```bash
cfgs@ubuntu:~$ sudo apt-get install rng-tools
```
> __Nota__: Las _rng-tools_ son un conjunto de utilidades relacionadas con la generación de números aleatorios en el kernel, aumentando la entreopía (aleatoriedad recogida por un sistema para su uso en criptografía o para otros usos que requieran números aleatorios) haciendo más rápido al _/dev/random_ (es un archivo especial que sirve como generador de números aleatorios).

A continuación, ejecutar el siguiente comando justo antes de generar las claves:

```bash
cfgs@ubuntu:~$ sudo rngd -r /dev/urandom antes de generar las claves.
```

* _Fuente_: https://www.enmimaquinafunciona.com/pregunta/1509/gpg-no-suficiente-entropia

### Generar las claves

Para poder cifrar asimétricamente primero tenemos que crear la pareja de claves (pública y privada) con el comando __gpg --gen-key__.

```bash
cfgs@ubuntu:~$ gpg --gen-key
gpg (GnuPG) 1.4.11; Copyright (C) 2010 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Por favor seleccione tipo de clave deseado:
   (1) RSA y RSA (predeterminado)
   (2) DSA y Elgamal
   (3) DSA (sólo firmar)
   (4) RSA (sólo firmar)
¿Su selección?:
```

__GPG__ nos permite elegir el tipo de clave que queremos usar, hay opciones que solo permiten firmar y otras que permiten firmar y cifrar, en este caso usaremos DSA y Elgamal.

```bash
las claves DSA pueden tener entre 1024 y 3072 bits de longitud.
¿De qué tamaño quiere la clave? (2048)
```

Nos piden el tamaño de la clave que puede variar entre 1024 bits y 3072. La elección del tamaño de la clave es de libre elección, cuanto mayor sea mayor seguridad, en este caso eligiré la el tamaño por defectoque es 2048.

Lo siguiente  que nos pide es la fecha en la que expirará la clave, la información del emisor de la clave (nombre, mail y comentario) y por último nos pedirá la contraseña que salvaguarda la clave privada.

Tras generar las claves podemos verlas con el comando __gpg -k__ que nos muestra nuestro anillo de claves, lo importante de este paso es que veremos la identificación de cada una, que es necesaria para poderlas exportar y enviar.

```bash
cfgs@ubuntu:~$ gpg -k
/home/cfgs/.gnupg/pubring.gpg
------------------------------
pub   2048D/18384645 2017-12-01
uid                  Jonay Garcia (Clave para el usuario Jonay) <jonay.garcia@gmail.com>
sub   2048g/C4A9EA7A 2017-12-01
```

### Exportar y enviar la clave privada

El objetivo de esta pareja de claves es que cualquiera nos pueda mandar un archivo cifrado que solo veremos nosotros y esto se hace difundiendo la clave pública que acabamos de crear (la pública, nunca la privada), para exportarla en un archivo usaremos el comando __gpg -output [archivo destino] --export [ID de a clave pública]__ (la clave pública generada antes tiene el ID 18384645).

```bash
cfgs@ubuntu:~$ gpg --output CPub.gpg --export 18384645
cfgs@ubuntu:~$ ls
CPub.gpg
```

Este archivo ahora se puede difundir por el medio que queramos, tenemos que tener en cuenta que el único problema de seguridad que habría en difundir la clave es que alguien se hiciese pasar por otro al mandarnos un mensaje, algo que pasaría igual si no estuviese cifrado, por eso el que nos envíe algo lo debería de firmar (si fuese pertinente).


### Subir una clave pública a un servidor de claves

Los servidores de claves suelen ser de acceso público (al no haber mucho problema por difundir una clave pública) y en este caso subiremos una clave a los servidores del MIT (pgp.mit.edu) usando el comando __gpg --send-keys --keyserver [Dirección del servidor] [ID de la clave pública]__ (al igual que antes el ID es 18384645).

```bash
cfgs@ubuntu:~$ gpg --send-keys --keyserver pgp.mit.edu 18384645
gpg: enviando clave 18384645 a hkp servidor pgp.mit.edu
```

A partir de este momento la clave estará accesible desde este servidor específico.

### Importar la clave desde el archivo o servidor de claves

Para poder usar la clave pública para cifrar o comprobar la identidad del remitente tenemos que importar previamente la clave.

Para importarla a partir de un archivo debemos de usar el comando __gpg --import [Archivo de la clave pública]__ (el que hemos generado anteriormente).

```bash
cfgs@ubuntu:~$ gpg --import CPub.gpg
gpg: clave 18384645: «Jonay Garcia (Clave para el usuario Jonay) <jonay.garcia@gmail.com>» sin cambios
gpg: Cantidad total procesada: 1
gpg:              sin cambios: 1
```

Al tener la clave ya en mi anillo de claves me contesta que no hay cambios.

Para importarla a partir del servidor de claves tenemos que usar el comando __gpg --keyserver [Dirección del servidor] --recv-keys [ID de la clave]__:

```bash
cfgs@ubuntu:~$ gpg gpg --keyserver pgp.mit.edu --recv-keys 18384645
gpg: solicitando clave 18384645 de hkp servidor pgp.mit.edu
gpg: clave 18384645: «Jonay Garcia (Clave para el usuario Jonay) <jonay.garcia@gmail.com>» sin cambios
gpg: Cantidad total procesada: 1
gpg:              sin cambios: 1
```

Como podemos ver al tener ya la clave nos devuelve el mismo mensaje.

### Cifrar con la clave pública

Ahora tenemos que pensar que hemos importado una clave pública, por ejemplo de nuestro jefe y tenemos que mandarle un documento, para cifrar el documento usaremos el comando __gpg —encrypt —recipient [ID de la clave] [Archivo]__:

```bash
cfgs@ubuntu:~$ echo "Hello World!" > greeting.txt
cfgs@ubuntu:~$ gpg --encrypt --recipient 18384645 greeting.txt
cfgs@ubuntu:~$ ls
greeting.txt greeting.txt.gpg
```

Y ya tenemos el archivo listo para mandarlo de forma segura.

### Descifrar un archivo con la clave privada

Y ahora es el momento de descifrar con nuestra clave privada el documento tras recibirlo, con el comando __gpg -d [Archivo]__ e introduciendo la contraseña que creamos para salvaguardar la clave privada.

```bash
cfgs@ubuntu:~$ gpg -d documento.txt.gpg
Necesita una frase contraseña para desbloquear la clave secreta
del usuario: "Jonay Garcia (Clave para el usuario Jonay) <jonay.garcia@gmail.com>"
clave ELG-E de 2048 bits, ID C4A9EA7A, creada el 2017-12-01 (ID de clave primaria 18384645)
gpg: cifrado con clave ELG-E de 2048 bits, ID C4A9EA7A, creada el 2017-12-01
      «Jonay Garcia (Clave para el usuario Jonay) <jonay.garcia@gmail.com>»
Hello World!
```

Y el resultado nos lo muestra a continuación __Hello World!__, aunque si queremos especificar la salida debemos de usar el parámetro __-o [Archivo de salida]__.

### Firmar archivos

Una de las medidas de seguridad básicas al pasar un mensaje es asegurarnos que el emisor es quien dice ser, para asegurarnos de esto digitalmente existe la firma digital, en el artículo anterior expliqué como GPG usaba los hash para crear una firma simple, pero también podemos cifrarlo y a su vez firmarlo, que es lo que haremos con el comando gpg -u [ID de la clave privada] --output [Archivo resultante] --sign [Archivo para firmar] e introduciendo la contraseña de la clave privada.

```bash
cfgs@ubuntu:~$  echo "Hello World!" > firmar.txt
cfgs@ubuntu:~$ gpg -u C4A9EA7A --output firmar.txt.gpg --sign firmar.txt
Necesita una frase contraseña para desbloquear la clave secreta
del usuario: "Jonay Garcia (Clave para el usuario Jonay) <jonay.garcia@gmail.com>"
clave DSA de 2048 bits, ID 18384645, creada el 2017-12-01
```

Y ahora para asegurarse la confidencialidad del documento (ahora que esta firmado por nosotros) deberíamos de cifrarlo con la clave pública del destinatario.

### Verificar y descifrar un archivo firmado

Cualquiera con la clave pública asociada a la que ha firmado el documento puede leerlo, de la misma forma que desciframos un archivo __gpg -d [Archivo]__ o verificándolo únicamente con el comando __gpg --verify [Archivo]__.

```bash
cfgs@ubuntu:~$ gpg --verify firmar.txt.gpg
gpg: Firmado el mié 12 ene 2017 05:25:18 CET usando clave DSA ID 18384645
gpg: Firma correcta de «Jonay Garcia (Clave para el usuario Jonay) <jonay.garcia@gmail.com>»
```

Y el resultado es la información del remitente, que podéis comprobar vosotros con este archivo y con la clave pública de los pasos anteriores.

### Ejercicio

Crear un script en Bash que permita cifrar un documento utilizando clave simétrica. El comando para encriptar un fichero para que no pida el password a través del prompt es:

```bash
echo "cfgs" > gpg_password.txt
cat gpg_password.txt | gpg -c --no-tty --cipher-algo AES256 --passphrase-fd 0 --batch -o greeting.txt.gpg greeting.txt
rm -f greeting.txt
```

Crear un script en Bash que permita descifrar un documento utilizando la clave simétrica. El comando para desencriptar un fichero para que no pida el password a través del promp es:

```Bash
cfgs@ubuntu:~$ cat gpg_password.txt | gpg --passphrase-fd 0 --decrypt --batch -o greeting.txt greeting.txt.gpg
```

## Resumen

__GPG__ es una herramienta de cifrado muy potente y fácil de usar, que en principio, a la mayoría no nos hace falta, pero puede que se nos presente la necesidad de enviar algo por medio inseguros (porque no haya más remedio), haciéndolo de esta forma podremos hacerlo sin miedo ha que lean el contenido del archivo.

[img_01]: img/01.png "GPG"
[img_02]: img/02.png "GPG"
[img_03]: img/03.png "GPG"
